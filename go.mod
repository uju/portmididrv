module gitlab.com/ubunatic/portmididrv

require (
	github.com/rakyll/portmidi v0.0.0-20170620004031-e434d7284291
	gitlab.com/gomidi/midi v1.10.0
)
